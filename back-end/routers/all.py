from typing import cast
from sqlalchemy import String, and_, desc, func, or_, Float
from sqlalchemy.dialects.postgresql import ARRAY
from database.schemas import Disability, Locations, Resources
from flask import Blueprint, request, jsonify
from setup import Session
from common_funcs import related_instances, paginate

all_blueprint = Blueprint("all_blueprint", __name__)

@all_blueprint.route("/search", methods=["GET"])
def search_all():
    models = {
        Disability: "disability",
        Locations: "locations",
        Resources: "resources"
    }
    search_cols = {
        Disability: [
            "disability.name",
            "disability.description",
            "disability.symptoms",
            "disability.causes",
        ],
        Locations: [
            "locations.name",
            "locations.address"
        ],
        Resources: [
            "resources.name",
            "resources.product_category",
            "resources.limitations",
        ]
    }
    res = {"results": [], "total": 0}
    session = Session()

    # get search param
    query_string = request.args.get("query")
    if not query_string:
        query_string = ""

    for table in search_cols.keys():
        query = session.query(table)
        conditions = []
        for column in table.__table__.columns:
            if not str(column) in search_cols[table]:
                continue
            if isinstance(column.type, ARRAY):
                conditions.append(column.any(query_string))
            else:
                conditions.append(column.ilike(f"%{query_string}%"))

        final_condition = or_(*conditions)

        # filter by category
        category = request.args.get("category")
        if category:
            final_condition = and_(
                final_condition,
                func.array_to_string(table.categories, " ").ilike(f"%{category}%"),
            )

        filtered_query = query.filter(final_condition)

        # execute
        filtered_results = filtered_query.all()
        session.close()

        records = [row.as_dict() for row in filtered_results]
        for i in range(len(records)):
            records[i]["model"] = models[table]
        res["results"] += records
        res["total"] += len(records)

    page_size = request.args.get("page_size")
    if not page_size:
        page_size = 15
    res["results"] = paginate(res["results"], page_size)

    if not res:
        return jsonify({"error": "Disability not found"}), 404
    return jsonify(res)
