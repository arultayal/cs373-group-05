from typing import cast
from sqlalchemy import String, and_, desc, func, or_, Float
from sqlalchemy.dialects.postgresql import ARRAY
from database.schemas import Disability, Locations, Resources
from flask import Blueprint, request, jsonify
from setup import Session
from common_funcs import related_instances, paginate

disability_blueprint = Blueprint("disability_blueprint", __name__)


@disability_blueprint.route("/<int:id>", methods=["GET"])
def disability(id):
    session = Session()
    disability = session.query(Disability).get(id)
    session.close()
    if disability:
        return jsonify(disability.as_dict())
    return jsonify({"error": "Disability not found"}), 404


@disability_blueprint.route("/all", methods=["GET"])
def all_disabilities():
    session = Session()
    all_disabilities = session.query(Disability).all()
    page_size = request.args.get("page_size")
    session.close()

    res = {"results": []}
    if all_disabilities:
        # Convert SQLAlchemy model objects to dictionaries
        for disability in all_disabilities:
            res["results"].append(disability.as_dict())

        res["results"] = paginate(res["results"], page_size)
        res["total"] = len(all_disabilities)
        return jsonify(res)

    return jsonify({"error": "No disabilities found"}), 404


@disability_blueprint.route("/related/<int:id>", methods=["GET"])
def get_related_disability(id):
    session = Session()
    disability = session.query(Disability).get(id)
    page_size = request.args.get("page_size")

    if not disability:
        session.close()
        return jsonify({"error": "Disability not found"}), 404

    cats = disability.categories
    res = related_instances(session, Locations, Resources, cats, page_size)
    session.close()
    return jsonify(res)


@disability_blueprint.route("/search", methods=["GET"])
def search_disability():
    search_cols = [
        "disability.name",
        "disability.description",
        "disability.symptoms",
        "disability.causes",
    ]
    session = Session()

    # get search param
    query_string = request.args.get("query")

    query = session.query(Disability)

    search_in = request.args.get("search_in").lower()
    if search_in == "all":
        conditions = []
        for column in Disability.__table__.columns:
            if not str(column) in search_cols:
                continue
            if isinstance(column.type, ARRAY):
                conditions.append(column.any(query_string))
            else:
                conditions.append(column.ilike(f"%{query_string}%"))

        final_condition = or_(*conditions)
    elif isinstance(getattr(Disability, search_in).type, ARRAY):
        # final_condition = getattr(Disability, search_in).any(query_string)
        final_condition = func.array_to_string(
            getattr(Disability, search_in), " "
        ).ilike(f"%{query_string}%")
    else:
        final_condition = getattr(Disability, search_in).ilike(f"%{query_string}%")

    # filter by category
    category = request.args.get("category")
    if category:
        final_condition = and_(
            final_condition,
            func.array_to_string(Disability.categories, " ").ilike(f"%{category}%"),
        )

    filtered_query = query.filter(final_condition)

    # sort by
    sort_by = request.args.get("sort_by")
    if sort_by:
        sort_by = sort_by.lower()
        if sort_by != "none":
            column_name = sort_by[: sort_by.find(" ")]
            column = getattr(Disability, column_name)
            if column_name == "statistics":
                column = func.cast(column, Float)
            if "ascending" in sort_by:
                filtered_query = filtered_query.order_by(column)
            else:
                filtered_query = filtered_query.order_by(desc(column))

    # execute
    filtered_results = filtered_query.all()
    session.close()

    res = {}
    res["results"] = [row.as_dict() for row in filtered_results]
    res["total"] = len(res["results"])

    page_size = request.args.get("page_size")
    if not page_size:
        page_size = 15
    res["results"] = paginate(res["results"], page_size)

    if not res:
        return jsonify({"error": "Disability not found"}), 404
    return jsonify(res)
