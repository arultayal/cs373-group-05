from sqlalchemy import and_, desc, func, or_, Float
from sqlalchemy.dialects.postgresql import ARRAY
from database.schemas import Disability, Locations, Resources
from flask import Blueprint, request, jsonify
from setup import Session
from common_funcs import related_instances, paginate

resources_blueprint = Blueprint("resources_blueprint", __name__)


@resources_blueprint.route("/<int:id>", methods=["GET"])
def resources(id):
    session = Session()
    resource = session.query(Resources).get(id)
    session.close()
    if resource:
        return jsonify(resource.as_dict())
    return jsonify({"error": "Resource not found"}), 404


@resources_blueprint.route("/all", methods=["GET"])
def all_resources():
    session = Session()
    all_resources = session.query(Resources).all()
    page_size = request.args.get("page_size")
    session.close()

    res = {"results": []}
    if all_resources:
        # Convert SQLAlchemy model objects to dictionaries
        for resource in all_resources:
            res["results"].append(resource.as_dict())

        res["results"] = paginate(res["results"], page_size)
        res["total"] = len(all_resources)
        return jsonify(res)

    return jsonify({"error": "No resources found"}), 404


@resources_blueprint.route("/related/<int:id>", methods=["GET"])
def get_related_resource(id):
    session = Session()
    resource = session.query(Resources).get(id)
    page_size = request.args.get("page_size")

    if not resource:
        session.close()
        return jsonify({"error": "Resource not found"}), 404

    cats = resource.categories
    res = related_instances(session, Disability, Locations, cats, page_size)
    session.close()
    return jsonify(res)


@resources_blueprint.route("/search", methods=["GET"])
def search_resources():
    search_cols = [
        "resources.name",
        "resources.product_category",
        "resources.limitations",
    ]
    session = Session()

    # get search param
    query_string = request.args.get("query")

    query = session.query(Resources)

    search_in = request.args.get("search_in").lower()
    print("-----------------------")
    print(request.args.get("search_in"))
    if search_in == "all":
        conditions = []
        for column in Resources.__table__.columns:
            if not str(column) in search_cols:
                continue
            if isinstance(column.type, ARRAY):
                conditions.append(column.any(query_string))
            else:
                conditions.append(column.ilike(f"%{query_string}%"))

        final_condition = or_(*conditions)
    elif isinstance(getattr(Resources, search_in).type, ARRAY):
        final_condition = func.array_to_string(
            getattr(Resources, search_in), " "
        ).ilike(f"%{query_string}%")
    else:
        final_condition = getattr(Resources, search_in).ilike(f"%{query_string}%")

    # filter by category
    category = request.args.get("category")
    if category:
        final_condition = and_(
            final_condition,
            func.array_to_string(Resources.categories, " ").ilike(f"%{category}%"),
        )

    filtered_query = query.filter(final_condition)

    # sort by
    sort_by = request.args.get("sort_by")
    if sort_by:
        sort_by = sort_by.lower()
        if sort_by != "none":
            column_name = sort_by[: sort_by.find(" ")]
            column = getattr(Resources, column_name)
            if column_name == "price":
                column = func.cast(column, Float)
            if "ascending" in sort_by:
                filtered_query = filtered_query.order_by(column)
            else:
                filtered_query = filtered_query.order_by(desc(column))

    # execute
    filtered_results = filtered_query.all()
    session.close()

    res = {}
    res["results"] = [row.as_dict() for row in filtered_results]
    res["total"] = len(res["results"])

    page_size = request.args.get("page_size")
    if not page_size:
        page_size = 15
    res["results"] = paginate(res["results"], page_size)

    if not res:
        return jsonify({"error": "Resource not found"}), 404
    return jsonify(res)
