from sqlalchemy import and_, desc, func, or_, Float
from sqlalchemy.dialects.postgresql import ARRAY
from database.schemas import Disability, Locations, Resources
from flask import Blueprint, request, jsonify
from setup import Session
from common_funcs import related_instances, paginate

locations_blueprint = Blueprint("locations_blueprint", __name__)


@locations_blueprint.route("/<int:id>", methods=["GET"])
def locations(id):
    session = Session()
    location = session.query(Locations).get(id)
    session.close()
    if location:
        return jsonify(location.as_dict())
    return jsonify({"error": "Location not found"}), 404


@locations_blueprint.route("/all", methods=["GET"])
def all_locations():
    session = Session()
    all_locations = session.query(Locations).all()
    page_size = request.args.get("page_size")
    session.close()

    res = {"results": []}
    if all_locations:
        for location in all_locations:
            res["results"].append(location.as_dict())

        res["results"] = paginate(res["results"], page_size)
        res["total"] = len(all_locations)
        return jsonify(res)

    return jsonify({"error": "No locations found"}), 404


@locations_blueprint.route("/related/<int:id>", methods=["GET"])
def get_related_location(id):
    session = Session()
    location = session.query(Locations).get(id)
    page_size = request.args.get("page_size")

    if not location:
        session.close()
        return jsonify({"error": "Location not found"}), 404

    cats = location.categories
    res = related_instances(session, Disability, Resources, cats, page_size)
    session.close()
    return jsonify(res)


@locations_blueprint.route("/search", methods=["GET"])
def search_locations():
    search_cols = ["locations.name", "locations.address"]
    session = Session()

    # get search param
    query_string = request.args.get("query")

    query = session.query(Locations)

    search_in = request.args.get("search_in").lower()
    if search_in == "all":
        conditions = []
        for column in Locations.__table__.columns:
            if not str(column) in search_cols:
                continue
            conditions.append(column.ilike(f"%{query_string}%"))

        final_condition = or_(*conditions)
    else:
        final_condition = getattr(Locations, search_in).ilike(f"%{query_string}%")

    # filter by category
    category = request.args.get("category")
    if category:
        final_condition = and_(
            final_condition,
            func.array_to_string(Locations.categories, " ").ilike(f"%{category}%"),
        )

    filtered_query = query.filter(final_condition)

    # sort by
    sort_by = request.args.get("sort_by")
    if sort_by:
        sort_by = sort_by.lower()
        if sort_by != "none":
            column_name = sort_by[: sort_by.find(" ")]
            column = getattr(Locations, column_name)
            if column_name == "rating":
                column = func.cast(column, Float)
            if "ascending" in sort_by:
                filtered_query = filtered_query.order_by(column)
            else:
                filtered_query = filtered_query.order_by(desc(column))

    # execute
    filtered_results = filtered_query.all()
    session.close()

    res = {}
    res["results"] = [row.as_dict() for row in filtered_results]
    res["total"] = len(res["results"])

    page_size = request.args.get("page_size")
    if not page_size:
        page_size = 15
    res["results"] = paginate(res["results"], page_size)

    if not res:
        return jsonify({"error": "Location not found"}), 404
    return jsonify(res)
