from database.schemas import Disability, Locations, Resources


# Get Related Instances
def related_instances(session, table1, table2, categories, page_size):
    table_names = {
        Disability: "disabilities",
        Locations: "locations",
        Resources: "resources",
    }
    res = {}
    set1 = set()
    set2 = set()
    try:
        page_size = int(page_size)
    except:
        page_size = 20

    # Query records with categories matching any of the categories in cats
    for cat in categories:
        set1.update(session.query(table1).filter(table1.categories.any(cat)).all())
        set2.update(session.query(table2).filter(table2.categories.any(cat)).all())

    tables = {table_names[table1]: set1, table_names[table2]: set2}
    for key in tables:
        res[key] = {"results": [], "total": len(tables[key])}
        index = -1
        cnt = page_size
        for k in tables[key]:
            if cnt == page_size:
                res[key]["results"].append([])
                cnt = 0
                index += 1
            res[key]["results"][index].append(k.as_dict())
            cnt += 1
    return res


# take in array of objects
# return it as an array of arrays of objects of length page_size
def paginate(arr, page_size):
    try:
        page_size = int(page_size)
    except:
        page_size = 20
    res = []
    index = -1
    cnt = page_size
    for k in arr:
        if cnt == page_size:
            res.append([])
            cnt = 0
            index += 1
        res[index].append(k)
        cnt += 1
    return res
