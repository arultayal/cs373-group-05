from sqlalchemy import inspect
from setup import app, engine
from routers.disabilities import disability_blueprint
from routers.locations import locations_blueprint
from routers.resources import resources_blueprint
from routers.all import all_blueprint


@app.route("/", methods=["GET"])
def home():
    return "This is the InclusiAbilityAPI"


@app.route("/get_tables", methods=["GET"])
def get_tables():
    inspector = inspect(engine)
    schemas = inspector.get_schema_names()

    res = {}

    for schema in schemas:
        res[schema] = {}
        for table_name in inspector.get_table_names(schema=schema):
            columns = inspector.get_columns(table_name, schema=schema)
            column_info = {}
            for column in columns:
                column_info[column["name"]] = column["type"].__str__()
            res[schema][table_name] = column_info
    return res


app.register_blueprint(disability_blueprint, url_prefix="/disability")
app.register_blueprint(locations_blueprint, url_prefix="/locations")
app.register_blueprint(resources_blueprint, url_prefix="/resources")
app.register_blueprint(all_blueprint, url_prefix="/all")

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000, debug=True)
