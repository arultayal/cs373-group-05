from sqlalchemy.orm import declarative_base
from sqlalchemy import Column, Integer, String, Float, Text
from sqlalchemy.dialects.postgresql import ARRAY

Base = declarative_base()


class Disability(Base):
    __tablename__ = "disability"
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=True)
    description = Column(Text, nullable=True)
    symptoms = Column(ARRAY(String), nullable=True)
    categories = Column(ARRAY(String), nullable=True)
    statistics = Column(Float, nullable=True)
    causes = Column(ARRAY(String), nullable=True)
    image_url = Column(Text, nullable=True)
    video_url = Column(Text, nullable=True)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Locations(Base):
    __tablename__ = "locations"
    id = Column(String, primary_key=True)
    name = Column(String, nullable=True)
    address = Column(String, nullable=True)
    categories = Column(ARRAY(String), nullable=True)
    latitude = Column(Float, nullable=True)
    longitude = Column(Float, nullable=True)
    rating = Column(String, nullable=True)
    image_url = Column(Text, nullable=True)
    place_id = Column(String, nullable=True)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Resources(Base):
    __tablename__ = "resources"
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=True)
    link = Column(Text, nullable=True)
    categories = Column(ARRAY(String), nullable=True)
    price = Column(String, nullable=True)
    product_category = Column(String, nullable=True)
    limitations = Column(ARRAY(String), nullable=True)
    img_url = Column(Text, nullable=True)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
