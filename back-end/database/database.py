from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from schemas import Base, Disability, Locations, Resources
import json

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = (
    "postgresql://postgres:BgtAccvcR5IPBqdmlX8H@inclusiability.clq0y0246k7r.us-east-2.rds.amazonaws.com:5432/postgres"
)
db = SQLAlchemy(app)


def upload_resources_from_json(json_file):
    # Read the JSON file
    with open(json_file, "r") as file:
        data = json.load(file)["results"]

    with app.app_context():
        session = db.session

        # Iterate over the JSON data and create Disability objects
        i = 1
        for item in data:
            resource = Resources(
                id=i,
                name=item.get("name"),
                link=item.get("link"),
                categories=item.get("categories"),
                product_category=item.get("product_category"),
                limitations=item.get("limitations"),
                img_url=item.get("img_url"),
                price=item.get("price"),
            )
            i += 1
            # Add the Disability object to the session
            session.merge(resource)

        # Commit the session to save the changes to the database
        session.commit()


def upload_locations_from_json(json_file):
    # Read the JSON file
    with open(json_file, "r") as file:
        data = json.load(file)["results"]

    with app.app_context():
        session = db.session

        # Iterate over the JSON data and create Disability objects
        i = 1
        for item in data:
            location = Locations(
                id=i,
                name=item.get("name"),
                address=item.get("address"),
                latitude=item.get("latitude"),
                longitude=item.get("longitude"),
                categories=item.get("categories"),
                rating=item.get("rating"),
                place_id=item.get("place_id"),
                image_url=item.get("img_url"),
            )
            i += 1
            # Add the Disability object to the session
            session.add(location)

        # Commit the session to save the changes to the database
        session.commit()


def upload_disabilities_from_json(json_file):
    # Read the JSON file
    with open(json_file, "r") as file:
        data = json.load(file)["results"]

    with app.app_context():
        session = db.session
        i = 1
        for item in data:
            disability = Disability(
                id=i,
                name=item.get("name"),
                description=item.get("description"),
                symptoms=item.get("symptoms"),
                categories=item.get("categories"),
                statistics=item.get("statistics"),
                causes=item.get("causes"),
                image_url=item.get("img_url"),
                video_url=item.get("video_url"),
            )
            i += 1
            session.add(disability)
        session.commit()


def update_price_from_json(json_file):
    # Read the JSON file
    with open(json_file, "r") as file:
        data = json.load(file)["results"]

    with app.app_context():
        session = db.session
        for item in data:
            # Fetch the existing Disability object by id
            resource_id = item.get("id")
            resource = session.query(Resources).filter_by(id=resource_id).first()
            if resource:
                # Update the video_url attribute
                resource.price = item.get("price")
                session.commit()
            else:
                print(f"Disability with id {resource_id} not found.")


def update_resource_from_json(json_file):
    # Read the JSON file
    with open(json_file, "r") as file:
        data = json.load(file)["results"]

    with app.app_context():
        session = db.session
        for item in data:
            # Fetch the existing Disability object by id
            location_id = item.get("id")
            location = session.query(Locations).filter_by(id=location_id).first()
            if location:
                # Update the video_url attribute
                location.categories = item.get("categories")
                session.commit()
            else:
                print(f"Disability with id {location_id} not found.")


def delete_all(table):
    with app.app_context():
        session = db.session
        session.query(table).delete()
        session.commit()


# Call the function to upload data from disabilities.json into the database
if __name__ == "__main__":
    delete_all(Disability)
    upload_disabilities_from_json("../scraping/disabilities.json")
    # upload_locations_from_json('../scraping/places.json')
    # upload_resources_from_json('../scraping/resources.json')
    # update_price_from_json('../scraping/resources.json')
    # update_resource_from_json('../scraping/places.json')
    pass
