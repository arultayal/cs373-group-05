from sqlalchemy import create_engine, Column, Integer, String, Text
from sqlalchemy.ext.declarative import declarative_base

# Define base class for declarative table definitions
Base = declarative_base()

# Define Disabilities table
class Disabilities(Base):
    __tablename__ = 'disabilities'

    id = Column(Integer, primary_key=True)
    title = Column(String)
    type = Column(String)
    causes = Column(String)
    symptoms = Column(String)
    famous = Column(String)
    statistics = Column(String)
    link1 = Column(String)
    link2 = Column(String)
    name1 = Column(String)
    name2 = Column(String)
    imageUrl = Column(String)
    youtubeLink = Column(String)

# Define Locations table
class Locations(Base):
    __tablename__ = 'locations'

    id = Column(Integer, primary_key=True)
    title = Column(String)
    address = Column(String)
    zipcode = Column(String)
    url = Column(String)
    disabilities = Column(String)
    rating = Column(String)
    costs = Column(String)
    link1 = Column(String)
    link2 = Column(String)
    name1 = Column(String)
    name2 = Column(String)
    imageUrl = Column(String)
    youtubeLink = Column(String)
    description = Column(Text)

# Define Resources table
class Resources(Base):
    __tablename__ = 'resources'

    id = Column(Integer, primary_key=True)
    title = Column(String)
    disability = Column(String)
    obtain = Column(String)
    usage = Column(String)
    shoppingLink = Column(String)
    price = Column(String)
    link1 = Column(String)
    link2 = Column(String)
    name1 = Column(String)
    name2 = Column(String)
    imageUrl = Column(String)
    youtubeLink = Column(String)

# Create engine to connect to AWS RDS database
engine = create_engine('postgresql://@ability-database.cpu66g00ybpu.us-east-2.rds.amazonaws.com:5432/ability_database')

# Create tables in the database
Base.metadata.create_all(engine)
