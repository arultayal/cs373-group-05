# Back-end README

## Activating Venv
1. python3 -m venv venv
2. source venv/bin/activate
3. pip3 install -r requirements.txt
4. python3 main.py 
5. deactivate (to exit venv)

## EC2 Hosting w/ AWS
1. SSH INTO EC2 INSTANCE
 - ssh -i "cs378-backend.pem" ubuntu@ec2-3-22-187-62.us-east-2.compute.amazonaws.com

2. CD INTO BACKEND (CHECK BRANCH TOO)
 - cd cs373/cs373-group-05
 - git pull
 - cd back-end

3. STOP RUNNING DOCKER CONTAINER
 - docker stop $(docker ps -a -q)
 - docker rm $(docker ps -a -q)

4. BUILD DOCKERFILE
 - sudo docker build --tag backend --file ./Dockerfile .

5. RUN DOCKERFILE
 - sudo docker run --restart always --network=host -i -t -v /home/ubuntu/cs373/cs373-group-05/backend-ec2:/usr/python backend

6. RUN API
 - gunicorn -w 2 -b 0.0.0.0:8000 'main:app'

7. EXIT DOCKER WITHOUT STOPPING IT
 - CTRL+P CTRL+Q

### Code
#### I. SSH
```
ssh -i "cs378-backend.pem" ubuntu@ec2-3-22-187-62.us-east-2.compute.amazonaws.com;
```
#### II. Docker Stuff
```
cd cs373/cs373-group-05;
git pull;
cd back-end;
docker stop $(docker ps -a -q);
docker rm $(docker ps -a -q);
sudo docker build --tag backend --file ./Dockerfile .;
sudo docker run --restart always --network=host -i -t -v /home/ubuntu/cs373/cs373-group-05/backend-ec2:/usr/python backend;
gunicorn -w 2 -b 0.0.0.0:8000 'main:app';
```
#### III. On bash
```
gunicorn -w 2 -b 0.0.0.0:8000 'main:app'
```
#### IV. Exit
```
(CTRL + P) (CTRL + Q)
```
