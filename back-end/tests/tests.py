import json
import unittest
import requests
from urllib.parse import urlparse


class EndpointTestCase(unittest.TestCase):
    def setUp(self):
        self.BASE_URL = "https://api.inclusiability.me"

    """ 
    GET ALL Disabilities
    """

    def test_disabilities_endpoint_status_code(self):
        response = requests.get(self.BASE_URL + "/disability/all")
        self.assertEqual(response.status_code, 200)

    """ 
    GET Individual Disability
    """

    def test_individual_disability_endpoint_status_code(self):
        response = requests.get(self.BASE_URL + "/disability/1")
        self.assertEqual(response.status_code, 200)

    def test_individual_disability_endpoint_returns_correct_disability_info(self):
        response = requests.get(self.BASE_URL + "/disability/1")
        data = response.json()
        expected_keys = {
            "categories",
            "causes",
            "description",
            "id",
            "image_url",
            "name",
            "statistics",
            "symptoms",
            "video_url",
        }
        self.assertSetEqual(set(data.keys()), expected_keys)

        self.assertEqual(data["id"], 1)
        self.assertEqual(data["name"], "Addison's Disease")

    def test_individual_disability_endpoint_field_types(self):
        response = requests.get(self.BASE_URL + "/disability/1")
        data = response.json()
        self.assertIsInstance(data["name"], str)
        self.assertIsInstance(data["image_url"], str)

        self.assertTrue(data["id"] >= 0)
        self.assertIsInstance(data["symptoms"], list)

    """ 
    GET ALL Locations
    """

    def test_locations_endpoint_status_code(self):
        response = requests.get(self.BASE_URL + "/locations/all")
        self.assertEqual(response.status_code, 200)

    """
    GET Individual Location
    """

    def test_specific_locations_endpoint_status_code(self):
        response = requests.get(self.BASE_URL + "/locations/1")
        self.assertEqual(response.status_code, 200)

    def test_specific_locations_endpoint_returns_correct_locations(self):
        response = requests.get(self.BASE_URL + "/locations/1")
        data = response.json()
        expected_keys = {
            "address",
            "categories",
            "id",
            "image_url",
            "latitude",
            "longitude",
            "name",
            "place_id",
            "rating",
        }
        self.assertSetEqual(set(data.keys()), expected_keys)

        self.assertEqual(data["id"], 1)
        self.assertEqual(data["latitude"], 30.4470314)
        self.assertEqual(data["name"], "United Access")

    def test_specific_locations_endpoint_field_types(self):
        response = requests.get(self.BASE_URL + "/locations/1")
        data = response.json()
        self.assertIsInstance(data["id"], int)
        self.assertIsInstance(data["name"], str)
        self.assertIsInstance(data["image_url"], str)

    """
    GET ALL Resources
    """

    def test_resources_endpoint_status_code(self):
        response = requests.get(self.BASE_URL + "/resources/all")
        self.assertEqual(response.status_code, 200)

    """
    GET individual Resources
    """

    def test_resources_individual_endpoint_structure(self):
        response = requests.get(self.BASE_URL + "/resources/1")
        self.assertEqual(response.status_code, 200)

        data = response.json()
        expected_keys = {
            "categories",
            "id",
            "img_url",
            "limitations",
            "link",
            "name",
            "price",
            "product_category",
        }
        self.assertSetEqual(set(data.keys()), expected_keys)

    def test_resources_individual_data_types(self):
        response = requests.get(self.BASE_URL + "/resources/1")
        self.assertEqual(response.status_code, 200)

        data = response.json()
        self.assertIsInstance(data["img_url"], str)
        self.assertIsInstance(data["id"], int)
        self.assertIsInstance(data["categories"], list)


if __name__ == "__main__":
    unittest.main()
