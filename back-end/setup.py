from flask import Flask
from flask_cors import CORS
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from database.schemas import Disability, Locations, Resources

app = Flask(__name__)
CORS(app)

DATABASE_URI = "postgresql://postgres:BgtAccvcR5IPBqdmlX8H@inclusiability.clq0y0246k7r.us-east-2.rds.amazonaws.com:5432/postgres"
engine = create_engine(DATABASE_URI)
Session = sessionmaker(bind=engine)
