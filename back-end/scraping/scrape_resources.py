from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import json
from scrape_disabilities import assign_categories


def exit_tab(f):
    def g(resource_elem, driver):
        res = f(resource_elem, driver)
        driver.close()
        driver.switch_to.window(driver.window_handles[-1])
        return res

    return g


def search_amazon(driver, query, categories, product_category, limitations):
    # driver.find_element(By.TAG_NAME, 'body').send_keys(Keys.CONTROL + 't')
    # driver.switch_to.window(driver.window_handles[-1])
    driver.get(f"https://www.amazon.com/s?k={query}")

    link_elem = driver.find_element(
        By.XPATH, "//div[@data-component-type='s-search-result']"
    )
    link_elem = link_elem.find_element(By.TAG_NAME, "a")
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable(link_elem))
    link_elem.send_keys(Keys.RETURN)
    res = {}
    try:
        WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "productTitle"))
        )
        res["name"] = driver.find_element(By.ID, "productTitle").text
        res["link"] = driver.current_url
        res["categories"] = categories.copy()
        res["product_category"] = product_category
        res["limitations"] = limitations
        res.update(scrape_amazon(driver))
    except:
        pass
    return res


def get_categories(driver):
    disability_elems = driver.find_element(By.CLASS_NAME, "col-md-4").find_elements(
        By.TAG_NAME, "a"
    )
    limitations = [elem.text for elem in disability_elems]
    cats = set()
    for limitation in limitations:
        cats.update(assign_categories(limitation, limitation))
    return list(cats), limitations


def scrape_amazon_new_tab(driver, link_elem):
    link_elem.send_keys(Keys.CONTROL + Keys.RETURN)
    driver.switch_to.window(driver.window_handles[-1])
    res = scrape_amazon(driver)
    driver.close()
    driver.switch_to.window(driver.window_handles[-1])
    return res


def scrape_amazon(driver):
    res = {}
    try:
        WebDriverWait(driver, 10).until(EC.title_contains("Amazon"))
        print("Page fully loaded.")
    except:
        #     print("Timed out waiting for page to load.")
        # if 'Amazon' not in driver.find_element(By.TAG_NAME, 'title').text:
        return res
    res["img_url"] = (
        driver.find_element(By.ID, "imgTagWrapperId")
        .find_element(By.TAG_NAME, "img")
        .get_attribute("src")
    )
    price = -1
    try:
        price = float(driver.find_element(By.CLASS_NAME, "a-price-whole").text)
        price += (
            float(driver.find_element(By.CLASS_NAME, "a-price-fraction").text) / 100
        )
    except:
        try:
            price = float(
                driver.find_elements_by_xpath("//*[contains(@class, 'a-price')]")[
                    0
                ].text[1:]
            )
        except:
            print("unavailable")

    if price != -1:
        res["price"] = price
    res["availability"] = price != -1

    return res


@exit_tab
def scrape_resource(resource_elem, driver):
    res = []
    # open link to each resource
    link_elem = resource_elem.find_element(By.TAG_NAME, "a")
    product_category = link_elem.text
    web_link = link_elem.get_attribute("href")
    link_elem.send_keys(Keys.CONTROL + Keys.RETURN)
    print("Opened URL:", web_link)

    # Switch to the new tab
    driver.switch_to.window(driver.window_handles[-1])

    # Check for limitations
    cats, limitations = get_categories(driver)
    if len(cats) == 0:
        return res

    try:
        vendors_and_products = driver.find_element(
            By.CLASS_NAME, "vendor-products"
        ).find_elements(By.CSS_SELECTOR, ".row.vendorRow")
    except:
        return res

    amazon_index = -1
    for i in range(len(vendors_and_products)):
        vendor = vendors_and_products[i].find_element(
            By.CSS_SELECTOR, ".col-xs-12.col-sm-6.vendor"
        )
        try:
            vendor = vendor.find_element(By.TAG_NAME, "a").text
        except:
            try:
                vendor = vendor.text
            except:
                print("could not find vendor")
                continue
        if "Amazon" in vendor:
            amazon_index = i
            break

    if amazon_index == -1:
        res.append(
            search_amazon(driver, product_category, cats, product_category, limitations)
        )
        return res
    try:
        products = (
            vendors_and_products[amazon_index]
            .find_element(By.CLASS_NAME, "products")
            .find_elements(By.TAG_NAME, "a")
        )
    except:
        return res

    for p in products:
        prod = {}
        prod["name"] = p.text
        prod["link"] = p.get_attribute("href")
        prod["categories"] = cats.copy()
        prod["product_category"] = product_category
        prod["limitations"] = limitations
        prod.update(scrape_amazon_new_tab(driver, p))
        res.append(prod)
        print(prod)

    return res


def redo(json_file):
    driver = webdriver.Chrome()
    with open(json_file, "r") as f:
        data = json.load(f)
    for prod in data["results"]:
        try:
            driver.get(prod["link"])
            prod.update(scrape_amazon(driver))
        except:
            pass

    with open("debug.json", "w") as f:
        json.dump(data, f, ensure_ascii=True, indent=2)


def main():
    options = webdriver.ChromeOptions()

    driver = webdriver.Chrome(options=options)
    driver.get("https://askjan.org/solutions/")

    result = {"results": []}
    resource_list = driver.find_element(By.ID, "solutions-154251-list-wrap")
    resources = resource_list.find_elements(By.CSS_SELECTOR, ".ta-item.flexitem")

    for resource_elem in resources:
        try:
            resources = scrape_resource(resource_elem, driver)
            result["results"] += resources
        except:
            print("error")

    with open("debug.json", "w") as f:
        json.dump(result, f, ensure_ascii=True, indent=2)


def remove_unavailable(json_file):
    with open(json_file, "r") as f:
        data = json.load(f)
    new_data = {"results": []}
    for prod in data["results"]:
        try:
            if prod["availability"]:
                new_data["results"].append(prod)
        except:
            print(prod)
    with open("debug.json", "w") as f:
        json.dump(new_data, f, ensure_ascii=True, indent=2)


if __name__ == "__main__":
    # main()
    remove_unavailable("debug.json")
