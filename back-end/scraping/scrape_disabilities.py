from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import json
import requests
import google.generativeai as genai

from config import GEMINI_API_KEY, GOOGLE_API_KEY, cx


def ask_gemini(name):
    genai.configure(api_key=GEMINI_API_KEY)
    model = genai.GenerativeModel("gemini-pro")
    response = {}
    response["statistics"] = model.generate_content(
        f'Give me a percentage estimate of the percent of the world population that has {name}. Only provide me this information as a decimal, do not provide any other text. For instance, if 0.0001% of the world has Addison\'s disease, please return ".000001"'
    ).text
    try:
        response["causes"] = model.generate_content(
            f'Give me the causes for {name}. Return it as a comma separated list with no other text included, just the causes. For instance, if i ask for the causes of Autism you should return: "Autoimmune disorders, Tuberculosis, Certain infections like HIV/AIDS"\n\nRETURN A COMMA SEPARATED LIST! Please do not include any sexually explicit or hamrful content in your response, just omit it.'
        ).text.split(", ")
    except:
        response["causes"] = "Harmful content detected"
    return response


def get_urls(query, num=1):
    res = {}
    img_api_url = f"https://www.googleapis.com/customsearch/v1?key={GOOGLE_API_KEY}&cx={cx}&searchType=image&q={query}&num={num}"
    response = requests.get(img_api_url)
    data = response.json()

    if "items" in data:
        res["image_url"] = data["items"][0]["link"]

    vid_api_url = f"https://www.googleapis.com/youtube/v3/search?key={GOOGLE_API_KEY}&part=snippet&type=video&q={query}&maxResults={num}"
    response = requests.get(vid_api_url)
    data = response.json()
    if "items" in data:
        vid_id = data["items"][0]["id"]["videoId"]
        res["video_url"] = f"https://www.youtube.com/watch?v={vid_id}"
    print(res)
    return res


# scrape an individual site within the main site
def scrape_disability(disability_elem, driver, name, access_apis=True):
    # open link to each disability
    link_elem = disability_elem.find_element(By.TAG_NAME, "a")
    disability_link = link_elem.get_attribute("href")
    link_elem.send_keys(Keys.CONTROL + Keys.RETURN)

    print("Opened URL:", disability_link)

    # Switch to the new tab
    driver.switch_to.window(driver.window_handles[-1])
    res = {}

    # Get description
    disability_content = driver.find_element(
        By.CSS_SELECTOR, ".cs_control.CS_Element_Custom"
    )
    res["description"] = disability_content.find_element(By.TAG_NAME, "p").text

    # Get symptoms/impairments
    symptoms = []
    try:
        symptom_table = disability_content.find_element(By.ID, "tab-by-limitation")
        symptom_elems = symptom_table.find_elements(By.CLASS_NAME, "acctitle")
        for s in symptom_elems:
            symptom_text = s.text
            symptoms.append(symptom_text)
    except:
        pass
    if len(symptoms) == 0:
        symptoms.append("No symptoms found")
    res["symptoms"] = symptoms

    # Get Work Related Functions
    wr_functions = []
    # open the tab in the table
    try:
        wr_tab = disability_content.find_element(By.ID, "tab-by-jf")
        wr_tab.click()

        wr_table = disability_content.find_element(By.ID, "tab-by-jobfunction")
        wr_elems = wr_table.find_elements(By.CLASS_NAME, "acctitle")
        for wr in wr_elems:
            wr_text = wr.text
            wr_functions.append(wr_text)
    except:
        pass

    if len(wr_functions) == 0:
        wr_functions.append("No work-related functions found")
    res["work_related_functions"] = wr_functions

    res["categories"] = assign_categories(name, symptoms)

    if access_apis:
        # Google Gemini
        res.update(ask_gemini(name))

        # Get image and video urls with google apis
        urls = get_urls(name, 1)
        res.update(urls)

    driver.close()
    driver.switch_to.window(driver.window_handles[0])
    return res


def scrape_main(access_apis=True):
    options = webdriver.ChromeOptions()
    # options.add_argument('--headless')

    driver = webdriver.Chrome(options=options)
    driver.get("https://askjan.org/a-to-z.cfm")

    result = {"results": []}
    disability_list = driver.find_element(By.ID, "disability-list")
    disabilities = disability_list.find_elements(By.CSS_SELECTOR, ".ta-item.flexitem")

    for disability_elem in disabilities:
        try:
            name = disability_elem.text

            try:
                dis_result = scrape_disability(
                    disability_elem, driver, name, access_apis
                )
                result["results"].append({"name": name, **dis_result})
            except:
                print("error at", name)
        except:
            print(disability_elem.text)

    with open("disabilities.json", "w") as f:
        json.dump(result, f, ensure_ascii=False, indent=2)


def assign_categories(name, symptoms):
    cat = []
    mobility = [
        "Standing",
        "Walking",
        "Sitting",
        "Lifting",
        "Balancing",
        "Overall Body Coordination",
        "Decreased Stamina/Fatigue",
        "Overall Body Weakness/Strength",
        "Use of One Hand/Arm",
        "Reaching",
    ]
    brain = [
        "Memory Loss",
        "Mental Confusion",
        "Organizing/Planning/Prioritizing",
        "Lifting",
        "Balancing",
    ]
    speech = [
        "No Speech",
        "Unintelligible Speech",
        "Weak Speech",
        "Stuttering Speech Disfluency",
    ]
    vision = [
        "Low Vision",
        "Photosensitivity",
        "Reading",
    ]
    hearing = ["Noise Sensitivity"]
    if "autism" in name.lower():
        cat.append("Autism")
    if any(target in symptoms for target in mobility):
        cat.append("Physical Impairment")
    if any(target in symptoms for target in hearing) or any(
        "Deaf" in item for item in symptoms
    ):
        cat.append("Hearing Impairment")
    if (
        any(target in symptoms for target in vision)
        or any("Blind" in item for item in symptoms)
        or any("Vision" in item for item in symptoms)
        or any("Visual" in item for item in symptoms)
    ):
        cat.append("Visual Impairment")
    if any(target in symptoms for target in brain):
        cat.append("Brain Impairment")
    if any(target in symptoms for target in speech):
        cat.append("Speech/Language Impairment")

    if len(cat) == 0:
        cat.append("Other")
    return cat


if __name__ == "__main__":
    scrape_main(True)
    pass
