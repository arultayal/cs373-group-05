from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import json
import googlemaps
import requests

from config import GOOGLE_API_KEY, cx


def get_data_from_json(json_file):
    with open(json_file, "r") as f:
        data = json.load(f)
    return data


def get_wheelchair_accessible():
    gmaps = googlemaps.Client(key=GOOGLE_API_KEY)
    places_result = gmaps.places(
        query="wheelchair accessible", location="Austin, TX", radius=10000
    )["results"]
    places_result += gmaps.places(
        query="wheelchair accessible entrance", location="Austin, TX", radius=10000
    )["results"]

    accessible_places = []
    for place in places_result:
        place_name = place["name"]
        place_location = place["geometry"]["location"]
        categories = ["Physical Impairment"]
        accessible_places.append(
            {
                "name": place_name,
                "address": place["formatted_address"],
                "latitude": place_location["lat"],
                "longitude": place_location["lng"],
                "categories": categories,
                "rating": place["rating"],
                "place_id": place["place_id"],
            }
        )
    return accessible_places


def dump_json(json_name, it):
    with open(json_name, "w") as f:
        json.dump(it, f, ensure_ascii=False, indent=2)


def main():
    place_list = get_data_from_json("wheelchair.json")
    data = get_data_from_json("places.json")["results"]
    gmaps = googlemaps.Client(key=GOOGLE_API_KEY)

    res = []
    for p in data:
        place = gmaps.places(query=p["names"], location=p["descriptions"])["results"][0]
        place_name = place["name"]
        place_location = place["geometry"]["location"]
        res.append(
            {
                "name": place_name,
                "address": place["formatted_address"],
                "latitude": place_location["lat"],
                "longitude": place_location["lng"],
                "categories": [],
                "rating": place["rating"],
                "place_id": place["place_id"],
            }
        )
    place_list += res
    dump_json("debug.json", place_list)


def add_img(json_file):
    gmaps = googlemaps.Client(key=GOOGLE_API_KEY)
    res = {"results": []}
    with open(json_file, "r") as f:
        data = json.load(f)

    for place in data["results"]:
        places_result = gmaps.places(
            query=f"{place['name']}", location=(place["latitude"], place["longitude"])
        )

        # Extract place ID
        place_id = places_result["results"][0]["place_id"]

        img_api_url = f"https://www.googleapis.com/customsearch/v1?key={GOOGLE_API_KEY}&cx={cx}&searchType=image&q={place['name']}&num=1"
        response = requests.get(img_api_url)
        img_response = response.json()["items"][0]["link"]
        res["results"].append(place | {"place_id": place_id, "img_url": img_response})

    with open("debug.json", "w") as f:
        json.dump(res, f, ensure_ascii=False, indent=2)


if __name__ == "__main__":
    # dump_json("wheelchair.json", get_wheelchair_accessible())
    # get_data_from_json("places.json")
    # main()
    add_img("places.json")
    pass
