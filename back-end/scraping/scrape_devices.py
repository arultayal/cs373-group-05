from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.chrome.options import Options
import json


def scrape_devices():

    PATH = "/Users/nickfavoriti/Documents/chromedriver-mac-x64/chromedriver"

    new = Service(PATH)
    browser_options = Options()
    browser_options.add_argument("--headless")
    browser_options.add_argument("--no-sandbox")

    driver = webdriver.Chrome(service=new, options=browser_options)
    driver.get(
        "https://www.freedommotors.com/16-must-have-smart-devices-for-the-disabled/"
    )

    result = {"results": []}
    devices = driver.find_elements(By.TAG_NAME, "h3")

    for i in range(len(devices)):
        name = devices[i].text
        result["results"].append(
            {
                "name": name,
            }
        )

    with open("texas_disabilities_devices.json", "w") as f:
        json.dump(result, f)

    driver.close()


if __name__ == "__main__":
    scrape_devices()
