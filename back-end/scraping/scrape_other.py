from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.chrome.options import Options
import json


def scrape_things_website():

    PATH = "/Users/nickfavoriti/Documents/chromedriver-mac-x64/chromedriver"

    s = Service(PATH)
    browser_options = Options()
    browser_options.add_argument("--headless")
    browser_options.add_argument("--no-sandbox")
    driver = webdriver.Chrome(service=s, options=browser_options)

    driver.get(
        "https://directory.dmagazine.com/search/?sections=Attractions&features=Wheelchair+Accessible"
    )

    result = {"results": []}
    activities = driver.find_elements(By.CLASS_NAME, "dir-card__title")
    descriptions = driver.find_elements(By.CLASS_NAME, "dir-card__text")
    for i in range(len(activities)):
        result["results"].append(
            {
                "names": activities[i].text,
                "descriptions": descriptions[i].text,
            }
        )
    with open("places.json", "w") as f:
        json.dump(result, f, ensure_ascii=False, indent=4)


if __name__ == "__main__":
    scrape_things_website()
