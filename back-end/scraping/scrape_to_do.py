from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.chrome.options import Options
import json


def scrape_things_website():

    PATH = "C:\Program Files (x86)\chromedriver.exe"

    s = Service(PATH)
    browser_options = Options()
    browser_options.add_argument("--headless")
    browser_options.add_argument("--no-sandbox")
    driver = webdriver.Chrome(service=s, options=browser_options)

    driver.get(
        "https://www.austintexas.org/austin-insider-blog/post/sensory-accessibility-austin/"
    )

    strong_activities = driver.find_elements(By.TAG_NAME, "strong")
    b_activities = driver.find_elements(By.TAG_NAME, "b")
    result = {"results": []}

    for activity in strong_activities:
        name = activity.find_element(By.CLASS_NAME, "name").text
        description = activity.find_element(By.CLASS_NAME, "description").text
        result["results"].append({"name": name, "description": description})
    with open("activities_for_disabled.json", "w") as f:
        json.dump(result, f, ensure_ascii=False, indent=4)

    driver.close()
