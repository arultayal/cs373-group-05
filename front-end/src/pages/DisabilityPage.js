import '../styles/InstancePage.css';
import React, { useState, useEffect } from 'react';
import LocationCard from '../components/LocationCard';
import ResourceCard from '../components/ResourceCard';
import CardGrid from '../components/CardGrid';
import YouTubeVideo from "../components/YouTubeVideo";
import Pagination from "../components/Pagination";

function DisabilityPage() {
    const [pageID, setPageID] = useState(0);
    const [member, setMember] = useState(null);
    const [relatedData, setRelatedData] = useState(null);

    // RELATED
    const [relatedPage, setRelatedPage] = useState(0);
    const [pageSize, setPageSize] = useState(2);
    const [totalRelated, setTotalRelated] = useState(0);

    useEffect(() => {
        const path = window.location.pathname;
        const parts = path.split('/');
        const last = parts[parts.length - 1];
        setPageID(last);
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch(`https://api.inclusiability.me/disability/${pageID}`);
                if (!response.ok) {
                    throw new Error('Failed to fetch data');
                }
                const jsonData = await response.json();
                setMember(jsonData);

                const relatedResponse = await fetch(`https://api.inclusiability.me/disability/related/${pageID}?page_size=${pageSize}`);
                if (!relatedResponse.ok) {
                    throw new Error('Failed to fetch data');
                }
                const relatedJSONData = await relatedResponse.json();
                setRelatedData(relatedJSONData);
                setTotalRelated(relatedJSONData["locations"]["total"] + relatedJSONData["resources"]["total"]);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        if (pageID !== 0) {
            fetchData();
        }
    }, [pageID]);

    return (
        <div className='instance-container'>
            {member ? (
                <>
                <h2 className='instance-header'>{member.name}</h2>
                <img src={member.image_url} className='instance-img'></img>
                <p className="card-text">
                    {member.description}
                </p>
                <p className="card-text">
                    <b>Categories: </b>{member.categories.join(', ')}
                </p>
                <p className="card-text">
                    <b>Symptoms: </b>{member.symptoms.join(', ')}
                </p>
                <p className="card-text">
                    <b>Causes: </b>{member.causes.join(', ')}
                </p>
                {/* <video width="750" height="500" controls >
                    <source src={member.video_url} type="video/mp4"/>
                </video> */}
                { member.video_url ?
                    <YouTubeVideo videoUrl={member.video_url} /> : <></>
                }
                
                </>) : <></>
            }

            <div style={{display: "grid", gridTemplateColumns: "repeat(2, auto)"}}>
                <CardGrid numCols={2}>
                    {relatedData && relatedData["locations"]["results"][relatedPage]
                        && relatedData["locations"]["results"][relatedPage].map((member) =>
                    (
                        <LocationCard member={member} />
                    ))}
                </CardGrid>
                <CardGrid numCols={2}>
                    {relatedData && relatedData["resources"]["results"][relatedPage] && relatedData["resources"]["results"][relatedPage].map((member) => (
                        <ResourceCard member={member} />
                    ))}
                </CardGrid>
            </div>
            <Pagination page={relatedPage} setPage={setRelatedPage} totalPages={Math.ceil(totalRelated / (2 * pageSize))}/>

        </div>
    );
}

export default DisabilityPage;