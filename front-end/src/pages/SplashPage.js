import '../styles/SplashPage.css';
// import { Link } from 'react-router-dom';
import YouTubeVideo from '../components/YouTubeVideo';

function SplashPage() { 
    return (
        <>
        <div className=''>
            <img
                className='splash-img vh-100'
                alt='disabilities'
                src='https://www.conveyormg.com/hs-fs/hubfs/accessibility%20best%20practices.jpg?width=1103&height=525&name=accessibility%20best%20practices.jpg'
                // src='https://www.bhmpics.com/downloads/disability-wallpaper/8.volunteers-helping-disabled-friends-outdoor-walking_74855-7933.jpg'
            />
            <div className='overlay-text'>
                <h1>
                    We are<br/>
                    InclusiAbility
                </h1>
                <p className='desc-text'>
                    We strive to provide an overview of what disabilities are and the diversity within this community. 
                    We also plan to include information about the various resources available for people in need,
                    such as accessible places/events and technologies/services that can assist those with disabilities.
                </p>
            </div>
            <h1 className='header-text'>Explore</h1>
            <div className='video-container'>
            <YouTubeVideo videoUrl='https://www.youtube.com/watch?v=qvUmGgk7HCs' opts={{width: '800px', height: '500px'}}></YouTubeVideo>
            </div>
        </div>
        </>
    );
}

export default SplashPage;