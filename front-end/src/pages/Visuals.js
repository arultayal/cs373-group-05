import React, { useEffect, useState } from 'react';
import Boxplot from '../components/Boxplot';
import BarChart from '../components/BarChart';
import PieChart from '../components/PieChart';
import '../styles/Visuals.css';

const Visual = () => {
    const [pieChartData, setPieChartData] = useState([]);
    const [boxplotData, setBoxplotData] = useState([]);
    const [barChartData, setBarChartData] = useState([]);

    // category data
    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch('https://api.inclusiability.me/disability/all?');
                if (!response.ok) {
                    throw new Error('Failed to fetch data');
                }
                const jsonData = await response.json();
                let categoryCounts = {};
                // Go through multiple for loops to grab the category a disability belongs to
                const categoryData = Object.values(jsonData)[0];
                categoryData.forEach(array => {
                    array.forEach(disability => {
                        const disabilityData = Object.values(disability)[0];
                        disabilityData.forEach(category => {
                            if (categoryCounts[category]) {
                                categoryCounts[category] += 1;
                              } else {
                                categoryCounts[category] = 1;
                              }
                        })
                    })
                  });

                // Convert the category counts object into an array of objects with name and value properties
                const pieChartData = Object.entries(categoryCounts).map(([name, value]) => ({
                    name,
                    value
                }));
                setPieChartData(pieChartData);
            } catch (error) {
                //console.error('Error fetching data:', error);
            }
        };

        fetchData();
    }, []);

    // rating data
    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch('https://api.inclusiability.me/locations/all?');
                if (!response.ok) {
                    throw new Error('Failed to fetch data');
                }
                const jsonData = await response.json();
                let ratingCounts = {};
                const ratingData = Object.values(jsonData)[0];
                // grab the ratings and round down for the values
                ratingData.forEach(array => {
                    array.forEach(location => {
                        const locationData = Object.values(location)[8];
                        const rating = Math.floor(locationData)
                        if (ratingCounts[rating]) {
                            ratingCounts[rating] += 1;
                          } else {
                            ratingCounts[rating] = 1;
                        }
                    })
                  });
                // Convert the category counts object into an array of objects with name and value properties
                const barChartData = Object.entries(ratingCounts).map(([name, value]) => ({
                    name,
                    value
                }));
                setBarChartData(barChartData);
            } catch (error) {
                //console.error('Error fetching data:', error);
            }
        };
        fetchData();
    }, []);

    // price data
    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch('https://api.inclusiability.me/resources/all?');
                if (!response.ok) {
                    throw new Error('Failed to fetch data');
                }
                const jsonData = await response.json();
                let pricingCounts = [];
                const pricingData = Object.values(jsonData)[0];
                console.log(Object.values(pricingData))
                pricingData.forEach(array => {
                    array.forEach(location => {
                        const locationData = Object.values(location)[6];
                        console.log(locationData)
                        const rating = locationData
                        pricingCounts.push(rating)
                    })
                });
                console.log(Object.values(pricingCounts))
                // Convert the category counts object into an array of objects with name and value properties
                const barChartData = Object.entries(pricingCounts).map(([name, value]) => ({
                    name,
                    value
                }));
                console.log('Pie chart data:', pricingCounts);
                setBoxplotData(pricingCounts);
            } catch (error) {
                //console.error('Error fetching data:', error);
            }
        };

        fetchData();
    }, []);

    return (
    <div>
        <div className="container mt-2">
        <div className="row justify-content-center">
            <div className="col-12 d-flex flex-column justify-content-center align-items-center">
                <h3 className="text-center mt-3 mb-3">Categories of Disabilities</h3>
                <PieChart data={pieChartData} width={500} height={500}/>
            </div>
            <div className="col-12 d-flex flex-column justify-content-center align-items-center mt-5">
                <h3 className="text-center mt-3">Resource Prices</h3>
                <Boxplot data={boxplotData}></Boxplot>
            </div>
            <div className="col-12 d-flex flex-column justify-content-center align-items-center mb-5">
                <h3 className="text-center mt-3">Rating of Events/Accessible Places</h3>
                <BarChart data={barChartData} title="Location Ratings" xaxis_label="Count" yaxis_label="Ratings"></BarChart>
            </div>
        </div>
            <h1 className="text-center mb-4">Self Critiques</h1>
            <p className="text-center">
                <strong>What did we do well?</strong>
                <br />
                We did well at communicating and meeting up to accomplish our work together.
                We were able to create a simple, intuitive design for the website that allows users
                to easily navigate this site.
            </p>

            <p className="text-center">
                <strong>What did we learn?</strong>
                <br />
                We learned a lot about full stack development. Most of us had never done this
                before, so we learned a lot from continuous integration to EC2 hosting. This was a very
                good learning experience for all of us.
            </p>

            <p className="text-center">
                <strong>What did we teach each other?</strong>
                <br />
                We shared knowledge on best practices in web development,
                database management, and cloud computing. Through collaboration
                and peer support, we enhanced our proficiency in coding,
                debugging, and optimizing performance, fostering a culture of
                continuous learning and improvement within the team.
            </p>

            <p className="text-center">
                <strong>What can we do better?</strong>
                <br />
                We can improve communication and project management to ensure
                smoother coordination and timely delivery of tasks.
                We also could have met up more in order to delegate and complete
                tasks more effectively.
            </p>

            <p className="text-center">
                <strong>What effect did the peer reviews have?</strong>
                <br />
                Peer reviews played a crucial role in maintaining code quality
                and ensuring adherence to accessibility standards. They provided
                 valuable feedback for refining our codebase, identifying areas
                 for optimization, and promoting a collaborative learning
                 environment where we could share constructive criticism and
                 insights.
            </p>

            <p className="text-center">
                <strong>What puzzles us?</strong>
                <br />
                We're still exploring optimal ways to balance accessibility
                requirements with aesthetic design elements. Additionally,
                we're curious about how to further optimize our website's
                performance and scalability, especially as our user base grows.
                Continuing to experiment and iterate will help us unravel these
                puzzles and enhance our website's effectiveness.
            </p>
        </div>
    </div>
    );
};

export default Visual;