import React, { useState, useEffect } from "react";
import '../styles/About.css';
import teamData from "../data/team_data.json";
import sourcesData from "../data/data_sources.json";
import toolsData from "../data/tools.json";
import axios from "axios";

function About() {
    const [totalCommits, setTotalCommits] = useState(0);
    const [totalIssues, setTotalIssues] = useState(0);
    const [commitsLoaded, setCommitsLoaded] = useState(false);
    const [issuesLoaded, setIssuesLoaded] = useState(false);

    // sets total commits and individual commits
    async function fetchCommits() {
        let totalCommitsCnt = 0;
        const response = await fetch(`https://gitlab.com/api/v4/projects/54615893/repository/contributors`);
        const jsonData = await response.json();
        
        teamData.forEach((teamMember) => {
            teamMember.commits = 0;
            for (const member of jsonData) {
                    if (member.name.toLowerCase().includes(teamMember.key_name)) {
                        teamMember.commits += member.commits;
                        totalCommitsCnt += member.commits;
                    }
            }
        });
        setTotalCommits(totalCommitsCnt);
        setCommitsLoaded(true);
    }

    // sets total issues and individual issues
    async function fetchIssues() {
        let issueMap = new Map();
        let totalIssuesCnt = 0;
        teamData.forEach((teamMember) => {
            issueMap.set(teamMember.gitlab_id, 0);
        });
        for (let pageNum = 1; pageNum < 4; pageNum++) {
            const response = await fetch(`https://gitlab.com/api/v4/projects/54615893/issues?per_page=100&page=${pageNum}`);
            const jsonData = await response.json();
            for (const member of jsonData) {
                if (member.state !== 'closed') continue;
                issueMap.set(member.closed_by.username, issueMap.get(member.closed_by.username) + 1);
            }
        }
        teamData.forEach((member) => {
            let issues = issueMap.get(member.gitlab_id);
            member.issues = issues;
            totalIssuesCnt += issues;
        });
        setTotalIssues(totalIssuesCnt);
        setIssuesLoaded(true);
    }

    useEffect(() => {
        if (!commitsLoaded) fetchCommits();
        if (!issuesLoaded) fetchIssues();
    }, [commitsLoaded, issuesLoaded]);

    return (
        <div className ="about-page-container">
            <h1>Our Goal</h1>
            <p className="goal-text">
                {/* Description of the site, its purpose, its intended users */}
                Our goal is to inform the public about the struggles that those
                with disabilities face in their day-to-day lives. We seek to help
                alleviate some of the work associated with having a disability
                by providing resources and accessible places/events for those
                with different types of disabilities. This site is primarily
                targeted towards those with disabilities seeking help or just the
                general public looking to learning more about those with disabilities.
            </p>

            <div className="people">
                <h1 className="about-header">Our Team</h1>
                <div className="about-row">
                    {teamData.map(member => (
                        <div className="card" key={member.name}>
                            <img src={member.img_src} className="card-img-top team-img" alt={member.name} />
                            <div className="card-body">
                                <h5 className="card-title">
                                    {member.name}
                                </h5>
                                <h6 className="text-muted">
                                    Role: {member.responsibilities}
                                </h6>
                                <p className="card-text">{member.description}</p>
                            </div>
                            <div className="card-footer">
                                <p>Gtilab ID: @{member.gitlab_id}</p>
                                <p>Issues Closed: {member.issues}</p>
                                <p>Commits: {member.commits}</p>
                                <p>Unit Tests: 8</p>
                                {/* <p>Unit Tests: {member.unit_tests}</p> */}
                            </div>
                        </div>
                    ))}
                </div>
                <center style={{marginTop: '20px'}}>
                    <p>
                        Total Commits: {totalCommits} <br/>
                        Total Issues: {totalIssues}
                    </p>
                </center>
                <div>
                    <h1 className="about-header">Data Sources</h1>
                    <ul>
                        {sourcesData.data_sources.map((member, index) => (
                            <li key={index}>
                                <a href={member} target="_blank" rel="noopener noreferrer">{member}</a>
                            </li>
                        ))}
                    </ul>
                </div>
                <div className="tools-container">
                    <h1 className="about-header">Tools Used</h1>
                    <div className="tool-grid">
                        {toolsData.map((member, index) => (
                            <div className="card tool-card" key={index}>
                                <img src={member.img_src} className="card-img-top tool-card-img" alt={member.name} />
                                <div className="card-body">
                                    <h5 className="card-title">{member.name}</h5>
                                    <p className="card-text">{member.description}</p>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default About;