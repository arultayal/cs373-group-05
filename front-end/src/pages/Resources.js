import React, { useState, useEffect } from 'react';
import ResourceCard from '../components/ResourceCard';
import CardGrid from '../components/CardGrid';
import Pagination from '../components/Pagination';
import SearchBar from '../components/SearchBar';
import Dropdown from '../components/Dropdown';
import {
    CATEGORIES,
    RESOURCE_SORT_BY,
    RESOURCE_SEARCH_BY
} from '../data/ModelData';

function Resources() {
    const [data, setData] = useState([]);
    const [page, setPage] = useState(0);
    const [pageSize, setPageSize] = useState(15);
    const [totalInstances, setTotalInstances] = useState(0);

    const [isLoaded, setIsLoaded] = useState(false);

    // FILTER + SORT BY OPTIONS
    const [query, setQuery] = useState("");
    const [category, setCategory] = useState(CATEGORIES[0]);
    const [sortBy, setSortBy] = useState(RESOURCE_SORT_BY[0]);
    const [searchBy, setSearchBy] = useState(RESOURCE_SEARCH_BY[0]);

    const fetchData = async () => {
        try {
            const response = await fetch(`https://api.inclusiability.me/resources/all?page_size=${pageSize}`);
            if (!response.ok) {
                throw new Error('Failed to fetch data');
            }
            const jsonData = await response.json();
            setData(jsonData);
            setTotalInstances(jsonData["total"]);
            setIsLoaded(true);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleSearch = async () => {
        try {
            let url = `https://api.inclusiability.me/resources/search?query=${query}&search_in=${searchBy}`;
            if (category !== "Any") {
                url += `&category=${category}`;
            }
            if (sortBy !== "None") {
                url += `&sort_by=${sortBy}`;
            }
            const response = await fetch(url);
            if (!response.ok) {
                throw new Error('Failed to fetch data');
            }
            const jsonData = await response.json();
            setData(jsonData);
            setPage(0);
            setTotalInstances(jsonData["total"]);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    return (
        <div className='model-page-container'>
            <div className='model-card-title'>
                <h1>Resources</h1>
                <p className='desc-text'>
                    Instances: {data["total"] && totalInstances}
                </p>
            </div>

            <div className='search-area'>
                <SearchBar 
                    handleSearch={handleSearch}
                    setQuery={setQuery}
                    setSearchBy={setSearchBy}
                    searchByList={RESOURCE_SEARCH_BY}
                />
                <div className='search-container dropdowns'>
                    <Dropdown
                        options={CATEGORIES}
                        setOption={setCategory}
                        optionText="Category"
                    />
                    <Dropdown
                        options={RESOURCE_SORT_BY}
                        setOption={setSortBy}
                        optionText="Sort By"
                    />
                </div>
            </div>
            
            { isLoaded ? <>
            {data && data["total"] > 0 ? 
                <CardGrid numCols={3}>
                    {data && data["total"] > 0 ? data["results"][page].map((member, index) => (
                        <ResourceCard key={index} member={member} searchWords={query} searchBy={searchBy}/>
                    )) : <></>
                }
                </CardGrid> : <h1 className="model-loading">No Resources Found</h1>
            }
            {
                totalInstances > 0 ? 
                <Pagination page={page} setPage={setPage} totalPages={Math.ceil(totalInstances / pageSize)}/>
                : <></>
            }
            </> : <h1 className="model-loading">Loading...</h1>}   
        </div>
    );
}

export default Resources;