import React, { useEffect, useState } from 'react';
import Treemap from '../components/Treemap';
import BarChart from '../components/BarChart';
import PieChart from '../components/PieChart';
import '../styles/Visuals.css';

const Provider = () => {
    const [pieChartData, setPieChartData] = useState([]);
    const [barChartData, setBarChartData] = useState([]);
    
    // author data
    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch('https://cs373-backend.ukrainecrisis.me/api/news');
                if (!response.ok) {
                    throw new Error('Failed to fetch data');
                }
                const jsonData = await response.json();
                let authorCounts = {
                "aol.com": 0,
                "businessinsider.com": 0,
                "feedfeeder": 0,
                "newsweek.com": 0,
                "ZEIT ONLINE: News -": 0,
                "zerohedge.com": 0,
                "Others": 0};

                // Checking for every category of authors the other website provided
                jsonData.forEach(array => {
                    const author = Object.values(array)[0];
                    if (author === "aol.com") {
                        authorCounts["aol.com"] += 1;
                    } else if (author === "businessinsider.com") {
                        authorCounts["businessinsider.com"] += 1;
                    } else if (author === "feedfeeder") {
                        authorCounts["feedfeeder"] += 1;
                    } else if (author === "newsweek.com") {
                        authorCounts["newsweek.com"] += 1;
                    } else if (author === "ZEIT ONLINE: News -") {
                        authorCounts["ZEIT ONLINE: News -"] += 1;
                    }else if (author === "zerohedge.com") {
                        authorCounts["zerohedge.com"] += 1;
                    } else {
                        authorCounts["Others"] += 1;
                    }
                  });

                // Convert the category counts object into an array of objects with name and value properties
                const pieChartData = Object.entries(authorCounts).map(([name, value]) => ({
                    name,
                    value
                }));
                setPieChartData(pieChartData);
            } catch (error) {
                //console.error('Error fetching data:', error);
            }
        };
        fetchData();
    }, []);

    // Ratings data
    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch('https://cs373-backend.ukrainecrisis.me/api/support-groups');
                if (!response.ok) {
                    throw new Error('Failed to fetch data');
                }
                const jsonData = await response.json();
                let ratingCounts = {};
                jsonData.forEach(array => {
                    const arrayData = Object.values(array)[5];
                    if (ratingCounts[arrayData]) {
                        ratingCounts[arrayData] += 1;
                      } else {
                        ratingCounts[arrayData] = 1;
                    }
                  });
                // Convert the category counts object into an array of objects with name and value properties
                const barChartData = Object.entries(ratingCounts).map(([name, value]) => ({
                    name,
                    value
                }));
                setBarChartData(barChartData);
            } catch (error) {
                //console.error('Error fetching data:', error);
            }
        };
        fetchData();
    }, []);

    return (
        <div>
        <div className="container mt-2">
        <div className="row justify-content-center">
            <div className="col-12 d-flex flex-column justify-content-center align-items-center">
                <h3 className="text-center mt-3 mb-3">Author/Website Distribution</h3>
                <PieChart data={pieChartData} width={500} height={500}/>
            </div>
            <div className="col-12 d-flex flex-column justify-content-center align-items-center mt-5">
                <h3 className="text-center mt-3">
                    Language Tree Map
                </h3>
                <Treemap />
            </div>
            <div className="col-12 d-flex flex-column justify-content-center align-items-center mb-5">
                <h3 className="text-center mt-3">Support Group Ratings</h3>
                <BarChart data={barChartData} xaxis_label="Count"></BarChart>
            </div>
        </div>
            <h1 className="text-center mb-4">Provider Critiques</h1>
            <p className="text-center">
                <strong>What did they do well?</strong>
                <br />
                They did well at communicating with our group and understanding
                what we wanted to see from their website. They did a great job
                with the design and models, as well as integrating different APIs.
            </p>

            <p className="text-center">
                <strong>How effective was their RESTful API?</strong>
                <br />
                Their RESTful API was pretty good. It provided a lot of information
                from their database, which was helpful for the visualizations 
            </p>

            <p className="text-center">
                <strong>How well did they implement your user stories?</strong>
                <br />
                They did really well at implementing our user stories.
                We think that they did a good job with finishing what we asked
                of them in a timely manner.
            </p>

            <p className="text-center">
                <strong>What did we learn from their website?</strong>
                <br />
                We learned a lot about the Ukrainian Refugee Crisis and the
                different support groups that are helping with this cause.
                While this may not be at the forefront of the news right now, it
                is still important to remember and stay up to date with.
            </p>

            <p className="text-center">
                <strong>What can they do better?</strong>
                <br />
                We think they could do a little better on spacing and the color
                choices. Some parts of the website have light text on light
                background, which makes it slightly more difficult to read.
            </p>

            <p className="text-center">
                <strong>What puzzles us about their website?</strong>
                <br />
                We are not puzzled by their website. It is a good website
                that conveys useful and detailed information. We think they
                did great.
            </p>
        </div>
        </div>
    );
};

export default Provider;