import '../styles/InstancePage.css';
import React, { useState, useEffect } from 'react';
import DisabilityCard from '../components/DisabilityCard';
import LocationCard from '../components/LocationCard';
import CardGrid from '../components/CardGrid';
import Pagination from '../components/Pagination';

function ResourcePage() {
    const [pageID, setPageID] = useState(0);
    const [member, setMember] = useState(null);
    const [relatedData, setRelatedData] = useState(null);
    
    // RELATED
    const [relatedPage, setRelatedPage] = useState(0);
    const [pageSize, setPageSize] = useState(2);
    const [totalRelated, setTotalRelated] = useState(0);

    useEffect(() => {
        const path = window.location.pathname;
        const parts = path.split('/');
        const last = parts[parts.length - 1];
        setPageID(last);
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch(`https://api.inclusiability.me/resources/${pageID}`);
                if (!response.ok) {
                    throw new Error('Failed to fetch data');
                }
                const jsonData = await response.json();
                setMember(jsonData);

                const relatedResponse = await fetch(`https://api.inclusiability.me/resources/related/${pageID}?page_size=${pageSize}`);
                if (!relatedResponse.ok) {
                    throw new Error('Failed to fetch data');
                }
                const relatedJSONData = await relatedResponse.json();
                setRelatedData(relatedJSONData);
                setTotalRelated(relatedJSONData["disabilities"]["total"] + relatedJSONData["locations"]["total"]);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        if (pageID !== 0) {
            fetchData();
        }
    }, [pageID]);

    return (
        <div className='instance-container'>
            {member &&
                <div className='desc-text'>
                    <h2 className='instance-header'>{member.name}</h2>
                    <img src={member.img_url} className='instance-img'></img>
                    <p className="card-text">
                        <b>Categories: </b>{member.categories.join(', ')}
                    </p>
                    <p className="card-text">
                        <b>Limitations: </b>{member.limitations.join(', ')}
                    </p>
                    <p className="card-text">
                        <b>Product Category: </b>{member.product_category}
                    </p>
                    <p className="card-text">
                        <b>Price: </b>${parseFloat(member.price).toFixed(2)}
                    </p>
                    <p className="card-text">
                        <b>Link: </b><a href={`${member.link}`} target="_blank">Buy Here</a>
                    </p>
                </div>
            }
            
            {/* <CardGrid numCols={4}>
                {relatedData && relatedData["disabilities"]["results"].map((page) => page.map((member) => (
                    <DisabilityCard member={member} />
                )))}
                {relatedData && relatedData["locations"]["results"].map((page) => page.map((member) => (
                    <LocationCard member={member} />
                )))}
            </CardGrid> */}

            
            <div style={{display: "grid", gridTemplateColumns: "repeat(2, auto)"}}>
                <CardGrid numCols={2}>
                    {relatedData && relatedData["disabilities"]["results"][relatedPage]
                        && relatedData["disabilities"]["results"][relatedPage].map((member) =>
                    (
                        <DisabilityCard member={member} />
                    ))}
                </CardGrid>
                <CardGrid numCols={2}>
                    {relatedData && relatedData["locations"]["results"][relatedPage]
                        && relatedData["locations"]["results"][relatedPage].map((member) => (
                        <LocationCard member={member} />
                    ))}
                </CardGrid>
            </div>
            <Pagination page={relatedPage} setPage={setRelatedPage} totalPages={Math.ceil(totalRelated / (2 * pageSize))}/>
        </div>
    );
}

export default ResourcePage;