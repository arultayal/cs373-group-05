import React, { useState, useEffect } from 'react';
import CardGrid from '../components/CardGrid';
import DisabilityCard from '../components/DisabilityCard';
import LocationCard from '../components/LocationCard';
import ResourceCard from '../components/ResourceCard';
import Pagination from '../components/Pagination';
import SearchBar from '../components/SearchBar';
import Dropdown from '../components/Dropdown';
import {
    CATEGORIES
} from '../data/ModelData';

function Search() {
    const [data, setData] = useState([]);
    const [page, setPage] = useState(0);
    const [pageSize, setPageSize] = useState(15);
    const [totalInstances, setTotalInstances] = useState(0);

    const [isLoaded, setIsLoaded] = useState(true);

    // FILTER + SORT BY OPTIONS
    const [query, setQuery] = useState("");
    const [category, setCategory] = useState(CATEGORIES[0]);

    const handleSearch = async () => {
        try {
            setIsLoaded(false);
            let url = `https://api.inclusiability.me/all/search?query=${query}`;
            if (category.toLowerCase() !== "any") {
                url += `&category=${category}`;
            }
            const response = await fetch(url);
            if (!response.ok) {
                throw new Error('Failed to fetch data');
            }
            const jsonData = await response.json();
            setData(jsonData["results"]);
            setPage(0);
            setTotalInstances(jsonData["total"]);
            setIsLoaded(true);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    return (
        <div className='model-page-container'>
            <div className='model-card-title'>
                <h1>General Search</h1>
                <p className='desc-text'>
                    Instances: {totalInstances}
                </p>
            </div>
            <div className='search-area'>
                <SearchBar
                    handleSearch={handleSearch}
                    setQuery={setQuery}
                />
                <div className='search-container dropdowns'>
                    <Dropdown
                        options={CATEGORIES}
                        setOption={setCategory}
                        optionText="Category"
                    />
                </div>
            </div>
            { isLoaded ? <>
            <CardGrid numCols={3}>
                {data && data[page] ? data[page].map((member) => {
                    if (member.model === "disability") {
                        return <DisabilityCard member={member} searchWords={query} searchBy="all"/>
                    } else if (member.model === "locations") {
                        return <LocationCard member={member} searchWords={query} searchBy="all"/>
                    } else {
                        return <ResourceCard member={member} searchWords={query} searchBy="all"/>
                    }
                }) : <></>}
            </CardGrid>
            
            {
                totalInstances > 0 ? 
                <Pagination page={page} setPage={setPage} totalPages={Math.ceil(totalInstances / pageSize)}/>
                : <></>
            }
            </> : <h1>Loading...</h1>}
        </div>
    );
}

export default Search;