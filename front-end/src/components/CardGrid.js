import '../styles/Model.css';

function CardGrid({children, numCols=3}) {
    let size = 95/numCols;
    return (
        <div className="model-card-row" style={{gridTemplateColumns: `repeat(${numCols}, ${size}%)`}}>
            {children}
        </div>
    );
}

export default CardGrid;