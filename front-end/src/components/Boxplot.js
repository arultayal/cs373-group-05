import React, { useRef, useEffect } from 'react';
import * as d3 from 'd3';

const Boxplot = ({ data, xaxis_label='' }) => {
  const d3Container = useRef(null);

  useEffect(() => {
    if (data && d3Container.current) {
      const svg = d3.select(d3Container.current)
        .attr("width", 500)
        .attr("height", 500);
      
      const margin = { top: 50, right: 50, bottom: 50, left: 50 };
      const width = +svg.attr("width") - margin.left - margin.right;
      const height = +svg.attr("height") - margin.top - margin.bottom;

      const g = svg.append("g")
        .attr("transform", `translate(${margin.left},${margin.top})`);

      // Compute summary statistics used for the box:
      const data_sorted = data.slice().sort(d3.ascending);
      const q1 = d3.quantile(data_sorted, 0.25);
      const median = d3.quantile(data_sorted, 0.5);
      const q3 = d3.quantile(data_sorted, 0.75);
      const iqr = q3 - q1;
      const min = q1 - 1.5 * iqr;
      const max = q3 + 1.5 * iqr;
      
      // Show the Y scale
      const y = d3.scaleLinear()
        .domain([0, 1000])
        .range([height, 0]);

      // Specify the tick values at intervals of 100
      const tickValues = d3.range(0, 1001, 100);
      g.append("g")
        .call(d3.axisLeft(y).tickValues(tickValues));

      // Draw box plot
      g.append("rect")
        .attr("x", width / 2 - 20)
        .attr("y", y(q3))
        .attr("width", 40)
        .attr("height", y(q1) - y(q3))
        .attr("stroke", "black")
        .style("fill", "purple");

      // Draw median line
      g.append("line")
        .attr("x1", width / 2 - 20)
        .attr("x2", width / 2 + 20)
        .attr("y1", y(median))
        .attr("y2", y(median))
        .attr("stroke", "black");

      // Draw vertical lines for whiskers
      g.selectAll(".whisker")
        .data([{ value: 0 }, { value: max }])
        .enter().append("line")
        .attr("class", "whisker")
        .attr("x1", width / 2)
        .attr("x2", width / 2)
        .attr("y1", d => y(d.value))
        .attr("y2", d => y(d.value))
        .attr("stroke", "black");
    }
  }, [data, xaxis_label]);

  return (
    <div className='flex-div'>
      <svg ref={d3Container} />
    </div>
  );
}

export default Boxplot;
