import '../styles/Navbar.css';
import { Link } from 'react-router-dom';

function Navbar() {
    return (
        <nav className="navbar fixed-top navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
            <Link to="/" className="navbar-brand">InclusiAbility</Link>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse ms-auto" id="navbarSupportedContent">
                <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                    <li className="nav-item">
                        <Link to='about' className="nav-link" aria-current="page">About</Link>
                    </li>
                    <li className="nav-item">
                        <Link to='disabilities' className="nav-link">Disabilities</Link>
                    </li>
                    <li className="nav-item">
                        <Link to='locations' className="nav-link">Events/Accessible Places</Link>
                    </li>
                    <li className="nav-item">
                        <Link to='resources' className="nav-link">Resources</Link>
                    </li>
                    <li className="nav-item">
                        <Link to='visuals' className="nav-link">Visuals</Link>
                    </li>
                    <li className="nav-item">
                        <Link to='provider' className="nav-link">Provider Visuals</Link>
                    </li>
                    <li className="nav-item">
                        <Link to='search' className="nav-link">Search</Link>
                    </li>
                </ul>
            </div>
        </div>
        </nav>
    );
}

export default Navbar;