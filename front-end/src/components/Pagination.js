import { Link } from 'react-router-dom';
import { GrFormPrevious, GrFormNext } from "react-icons/gr";

function Pagination({page, setPage, totalPages}) {
    // const [page, setPage] = useState(0);

    const prevPage = () => {
        if (page > 0) {
            setPage(page - 1);
        }
    }
 
    const nextPage = () => {
        if (page + 1 < totalPages) {
            setPage(page + 1);
        }
    }

    return (
        <nav aria-label="Page navigation example">
            <ul className="pagination justify-content-center">
                <li className="page-item">
                <Link className="page-link" onClick={prevPage} aria-label="Previous">
                    <span aria-hidden="true"><GrFormPrevious /></span>
                </Link>
                </li>
                <li className="page-item"><p className="page-link">{page + 1}</p></li>
                <li className="page-item"><p className="page-link">{totalPages}</p></li>
                <li className="page-item">
                <Link className="page-link" onClick={nextPage} aria-label="Previous">
                    <span aria-hidden="true"><GrFormNext /></span>
                </Link>
                </li>
            </ul>
        </nav>
    );
}

export default Pagination;