import '../styles/Model.css';
import { minimize } from '../components/ModelFunctions';
import { Link } from 'react-router-dom';
import Highlighter from '../components/Highlighter';

function ResourceCard({member, searchWords, searchBy}) {
    return (
        <div className="card model-card">
            <div className="card-body">
                <img className="card-img-top model-card-img-top" src={member.img_url} alt="Empty"/>
                <h5 className="card-title model-card-title">
                    <Highlighter 
                        searchWords={searchWords}
                        searchBy={searchBy}
                        type="name"
                        text={member.name && member.name.length > 50 ? member.name.slice(0, 50) + '...' : member.name}
                    />
                </h5>
                <p className="card-text">
                    <b>Categories: </b>{minimize(member.categories)}
                </p>
                <p className="card-text">
                    <b>Limitations: </b>
                    <Highlighter 
                        searchWords={searchWords}
                        searchBy={searchBy}
                        type="limitations"
                        text={minimize(member.limitations)}
                    />
                </p>
                <p className="card-text">
                    <b>Product Category: </b>
                    <Highlighter 
                        searchWords={searchWords}
                        searchBy={searchBy}
                        type="product_category"
                        text={member.product_category}
                    />
                </p>
                <p className="card-text">
                    <b>Price: </b>${parseFloat(member.price).toFixed(2)}
                </p>
                <p className="card-text">
                    <b>Link: </b><a href={`${member.link}`} target="_blank">Buy Here</a>
                </p>
                <div className="card-footer model-footer">
                    <Link to={`/resources/${member.id}`} className="btn btn-primary" state={{member}}>More Info</Link>
                </div>
            </div>
        </div>
    );
}

export default ResourceCard;