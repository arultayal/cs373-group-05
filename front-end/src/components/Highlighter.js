import Highlight from 'react-highlight-words';

function Highlighter({searchWords, searchBy, type, text}) {
    return (
        searchBy && (searchBy.toLowerCase() === "all" || type === searchBy) ?
            <Highlight 
                searchWords={[searchWords]}
                textToHighlight={text}
            /> : <>{text}</>
    );
}

export default Highlighter;