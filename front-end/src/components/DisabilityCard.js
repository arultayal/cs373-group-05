import '../styles/Model.css';
import { minimize } from '../components/ModelFunctions';
import { Link } from 'react-router-dom';
import { useState } from 'react';
import Highlighter from '../components/Highlighter';

function DisabilityCard({member, searchWords, searchBy}) {
    const [imageLoaded, setImageLoaded] = useState(true);

    const handleImageNotLoad = () => {
        setImageLoaded(false);
    }

    return (
        <div className="card model-card">
            <div className="card-body">
                {imageLoaded && <img className="card-img-top model-card-img-top"
                    src={member.image_url}
                    alt="Empty"
                    onError={handleImageNotLoad}
                />}
                <h5 className="card-title model-card-title">
                    <Highlighter 
                        searchWords={searchWords}
                        searchBy={searchBy}
                        type="name"
                        text={member.name}
                    />
                </h5>
                <p className="card-text">
                    <Highlighter 
                        searchWords={[searchWords]}
                        searchBy={searchBy}
                        type="description"
                        text={member.description && member.description.length > 50 ? member.description.slice(0, 50) + '...' : member.description}
                    />
                </p>
                <p className="card-text">
                    <b>Categories: </b>{minimize(member.categories)}
                </p>
                <p className="card-text">
                    <b>Symptoms: </b>
                    <Highlighter 
                        searchWords={[searchWords]}
                        searchBy={searchBy}
                        type="symptoms"
                        text={minimize(member.symptoms)}
                    />
                </p>
                <p className="card-text">
                    <b>Causes: </b>
                    <Highlighter 
                        searchWords={[searchWords]}
                        searchBy={searchBy}
                        type="causes"
                        text={minimize(member.causes)}
                    />
                </p>
                <p className="card-text">
                    <b>Population Stats: </b>{Math.round(member.statistics * 10000) / 100}%
                </p>
                <div className="card-footer model-footer">
                    <Link to={`/disabilities/${member.id}`} className="btn btn-primary" state={{ member }}>More Info</Link>
                </div>
            </div>
        </div>
    );
}

export default DisabilityCard;