import DisabilityCard from '../components/DisabilityCard';
import LocationCard from '../components/LocationCard';
import ResourceCard from '../components/ResourceCard';
import '../styles/RelatedGrid.css';

export const DISABILITY = "disability";
export const LOCATION = "location";
export const RESOURCE = "resource";

const cards = {
    DISABILITY: DisabilityCard,
    LOCATION: LocationCard,
    RESOURCE: ResourceCard,
}

function RelatedGrid({option1, option2, relatedData, relatedPage, setRelatedPage, pageSize1, pageSize2}) {
    return (
        <div>
            <CardGrid numCols={2}>
                {relatedData && relatedData[option1]["results"][relatedPage]
                    && relatedData[option1]["results"][relatedPage].map((member) =>
                (
                    <cards.option1 member={member} />
                ))}
            </CardGrid>
            <CardGrid numCols={2}>
                {relatedData && relatedData[option2]["results"][relatedPage]
                    && relatedData[option2]["results"][relatedPage].map((member) => (
                    <cards.option2 member={member} />
                ))}
            </CardGrid>
            <Pagination page={relatedPage} setPage={setRelatedPage} totalPages={Math.ceil(totalRelated / (2 * pageSize1))}/>
            <Pagination page={relatedPage} setPage={setRelatedPage} totalPages={Math.ceil(totalRelated / (2 * pageSize2))}/>
        </div>
    )
}

export default RelatedGrid;