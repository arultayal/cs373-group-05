
export function find_element(target, json_file) {
    let data = JSON.parse(json_file)["results"];
    for (var i = 0; i < data.length; i++) {
        if (data[i].name === target) {
            return [data[i], i + 1];
        }
    }
    return [null, -1];
}

export function minimize(elem_list) {
    return (
        elem_list.length > 3
            ? elem_list.slice(0, 3).join(', ') + ', ...'
            : elem_list.join(', ')
    );
}