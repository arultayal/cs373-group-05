import { useState } from "react";

function Dropdown({options, setOption, optionText}) {
    const [isOpen, setIsOpen] = useState(false);
    const [selectedOption, setSelectedOption] = useState(options[0]);

    const toggleDropdown = () => {
        setIsOpen(!isOpen);
    };
    
    const handleClick = (option) => {
        setSelectedOption(option);
        option = option.toLowerCase();
        if (option === "product category") {
            option = option.split(' ').join('_');
        }
        setOption(option);
        toggleDropdown();
    }

    return (
        <div className="dropdown">
            <button
                className="btn btn-primary dropdown-toggle"
                type="button"
                onClick={toggleDropdown}
            >
                {optionText}: {selectedOption}
            </button>
            <ul className={`dropdown-menu ${isOpen ? 'show' : ''}`}>
                {options.map((option) => {
                    return (
                        <li>
                            <button className="dropdown-item" onClick={() => handleClick(option)}>
                                {option}
                            </button>
                        </li>
                    );
                })}
            </ul>
        </div>
    );
}

export default Dropdown;