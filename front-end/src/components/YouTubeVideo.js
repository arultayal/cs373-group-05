import React from "react";
import YouTube from "react-youtube";

function YouTubeVideo({ videoUrl, opts }) {
  // Extract video ID from the YouTube URL
  const videoId = videoUrl.split("v=")[1];

  return (
    <div>
      <YouTube videoId={videoId} opts={opts} />
    </div>
  );
}

export default YouTubeVideo;
