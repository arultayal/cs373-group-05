import { useState } from "react";
import "../styles/Model.css"
import Dropdown from "./Dropdown";

function SearchBar({handleSearch, setQuery, setSearchBy, searchByList}) {

    const handleInputChange = (e) => {
        e.preventDefault();
        setQuery(e.target.value);
    }

    return (
        <div className="search-container">
            <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" onChange={handleInputChange}/>
            {searchByList ? 
            <Dropdown
                options={searchByList}
                optionText="Search By" 
                setOption={setSearchBy}/> : <></>
            }
            <button className="btn btn-outline-success my-2 my-sm-0" type="submit" onClick={handleSearch}>Search</button>
        </div>
    )    
}

export default SearchBar;