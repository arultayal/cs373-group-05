import '../styles/Model.css';
import { Link } from 'react-router-dom';
import { useState } from 'react';
import Highlighter from '../components/Highlighter';

function LocationCard({member, searchWords, searchBy}) {
    const [imageLoaded, setImageLoaded] = useState(true);

    const handleImageNotLoad = () => {
        setImageLoaded(false);
    }

    return (
        <div className="card model-card">
            <div className="card-body">
                {imageLoaded && <img className="card-img-top model-card-img-top"
                    src={member.image_url}
                    alt="Empty"
                    onError={handleImageNotLoad}
                />}
                
                <h5 className="card-title model-card-title">
                    <Highlighter 
                        searchWords={searchWords}
                        searchBy={searchBy}
                        type="name"
                        text={member.name}
                    />
                </h5>
                <p className="card-text">
                    <b>Address: </b>
                    <Highlighter 
                        searchWords={[searchWords]}
                        searchBy={searchBy}
                        type="address"
                        text={member.address}
                    />
                </p>
                <p className="card-text">
                    <b>Location: </b>{member.latitude}, {member.longitude}
                </p>
                <p className="card-text">
                    <b>Categories: </b>{member.categories}
                </p>
                <p className="card-text">
                    <b>Rating: </b>{member.rating}/5
                </p>
                <div className="card-footer model-footer">
                    <Link to={`/locations/${member.id}`} className="btn btn-primary" state={{ member }}>More Info</Link>
                </div>
            </div>
        </div>
    );
}

export default LocationCard;