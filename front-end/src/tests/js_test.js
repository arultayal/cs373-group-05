import { BrowserRouter} from 'react-router-dom';
import renderer from 'react-test-renderer';
import { render, screen, fireEvent, cleanup, waitFor } from '@testing-library/react';

import Navbar from '../components/Navbar.jsx';
import Circle from '../components/Circle.js';
import ModelFunctions from '../components/ModelFunctions.js'

import About from '../pages/About.js';
import Disabilities from '../pages/Disabilities.js';
import DisabilityPage from '../pages/DisabilityPage.js';
import LocationPage from '../pages/LocationPage.js';
import Locations from '../pages/Locations.js';
import ResourcePage from '../pages/ResourcePage.js';
import Resources from '../pages/Resources.js';
import SplashPage from '../pages/SplashPage.js';

afterEach(() => {
    cleanup();
});

it('Init NavBar' , () => {
    const component = renderer.create(
        <BrowserRouter>
            <Navbar />
        </BrowserRouter>
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

it('Init Circle' , () => {
    const component = renderer.create(
        <BrowserRouter>
            <Circle />
        </BrowserRouter>
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

it('Init ModelFunctions' , () => {
    const component = renderer.create(
        <BrowserRouter>
            <ModelFunctions />
        </BrowserRouter>
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

test('About renders', async () => {
    render(<BrowserRouter><About /></BrowserRouter>);
    await waitFor(() => {
        expect(screen.getByText(/Our Goal/)).toBeInDocument();
    });
});

test('Disabilities renders', async () => {
    render(<BrowserRouter><Disabilities /></BrowserRouter>);
    await waitFor(() => {
        expect(screen.getByText(/Disabilities/)).toBeInDocument();
    });
});

test('Locations renders', async () => {
    render(<BrowserRouter><Locations /></BrowserRouter>);
    await waitFor(() => {
        expect(screen.getByText(/Locations/)).toBeInDocument();
    });
});

test('Resources renders', async () => {
    render(<BrowserRouter><Disabilities /></BrowserRouter>);
    await waitFor(() => {
        expect(screen.getByText(/Resources/)).toBeInDocument();
    });
});

test('Splash renders', async () => {
    render(<BrowserRouter><SplashPage /></BrowserRouter>);
    await waitFor(() => {
        expect(screen.getByText(/We Are InclusAbility/)).toBeInDocument();
    });
});




