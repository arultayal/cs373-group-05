import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import SplashPage from './pages/SplashPage';
import Navbar from './components/Navbar';
import About from './pages/About';
import DisabilityPage from './pages/DisabilityPage';
import ResourcePage from './pages/ResourcePage';
import LocationPage from './pages/LocationPage';

import Disabilities from './pages/Disabilities';
import Locations from './pages/Locations';
import Resources from './pages/Resources';
import Search from './pages/Search';
import Visuals from './pages/Visuals';
import Provider from './pages/Provider';

function App() {
  return (
    <Router>
      <Navbar/>
      <Routes>
        <Route path='/' element={<SplashPage/>}/>
        <Route path='/about' element={<About/>}/>
        <Route path='/disabilities' element={<Disabilities/>}/>
        <Route path='/locations' element={<Locations/>}/>
        <Route path='/resources' element={<Resources/>}/>
        <Route path='/visuals' element={<Visuals/>}/>
        <Route path='/provider' element={<Provider/>}/>
        <Route path='/search' element={<Search/>}/>
        <Route exact path="/disabilities/:id" element={<DisabilityPage />} />
        <Route exact path="/locations/:id" element={<LocationPage />} />
        <Route exact path="/resources/:id" element={<ResourcePage />} />
      </Routes>
    </Router>
  );
}



export default App;
