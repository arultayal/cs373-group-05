
export const CATEGORIES = [
    "Any",
    "Autism",
    "Brain Impairment",
    "Hearing Impairment",
    "Physical Impairment",
    "Speech/Language Impairment",
    "Visual Impairment",
    "Other"
]

/* DISABILITY */

export const DISABILITY_SORT_BY = [
    "None",
    "Name Ascending",
    "Name Descending",
    "Statistics Ascending",
    "Statistics Descending"
]

export const DISABILITY_SEARCH_BY = [
    "All",
    "Name",
    "Description",
    "Symptoms",
    "Causes"
]

/* LOCATION */

export const LOCATION_SORT_BY = [
    "None",
    "Name Ascending",
    "Name Descending",
    "Rating Ascending",
    "Rating Descending"
]

export const LOCATION_SEARCH_BY = [
    "All",
    "Address",
    "Name",
]

/* RESOURCES */

export const RESOURCE_SORT_BY = [
    "None",
    "Name Ascending",
    "Name Descending",
    "Price Ascending",
    "Price Descending"
]

export const RESOURCE_SEARCH_BY = [
    "All",
    "Name",
    "Product Category",
    "Limitations"
]