from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import unittest
import re
import requests

URL = "https://www.inclusiability.me/"


class testcases(unittest.TestCase):
    def setUp(self):
        options = Options()
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-gpu")
        options.add_argument("--disable-dev-shm-usage")
        
        options.add_argument('log-level=3')

        # Needed for navbar to show up
        options.add_argument("--window-size=1920,1080")
        options.add_argument("--start-maximized")

        self.driver = webdriver.Chrome(options=options)
        self.driver.get(URL)

        self.wait = WebDriverWait(self.driver, 2)

    def tearDown(self):
        self.driver.quit()

    # Checks if the title is correct and if the home button redirects to the original URL
    def test_splash(self):
        print("test splash")
        self.assertEqual(self.driver.title, "InclusiAbility")
        self.driver.find_element(By.LINK_TEXT, "InclusiAbility").click()
        self.assertEqual(self.driver.current_url, URL)

    # Checks if the About button in the navbar redirects to the correct URL
    def test_nav1(self):
        print("test nav1")
        self.driver.find_element(By.LINK_TEXT, "About").click()
        self.assertEqual(self.driver.current_url, URL + "about")

    # Checks if the Disabilities button in the navbar redirects to the correct URL
    def test_nav2(self):
        print("test nav2")
        self.driver.find_element(By.LINK_TEXT, "Disabilities").click()
        self.assertEqual(self.driver.current_url, URL + "disabilities")

    # Checks if the Places button in the navbar redirects to the correct URL
    def test_nav3(self):
        print("test nav3")
        self.driver.find_element(By.LINK_TEXT, "Events/Accessible Places").click()
        self.assertEqual(self.driver.current_url, URL + "locations")

    # Checks if the Resources button in the navbar redirects to the correct URL
    def test_nav4(self):
        print("test nav4")
        self.driver.find_element(By.LINK_TEXT, "Resources").click()
        self.assertEqual(self.driver.current_url, URL + "resources")

    # Checks if the Search button in the navbar redirects to the correct URL
    def test_nav5(self):
        print("test nav5")
        self.driver.find_element(By.LINK_TEXT, "Search").click()
        self.assertEqual(self.driver.current_url, URL + "search")

    # Checks if there are 5 contributor boxes in the About page
    def test_contributors(self):
        print("test_contributors")
        self.driver.find_element(By.LINK_TEXT, "About").click()
        self.assertEqual(self.driver.page_source.count("Role:"), 5)

    # Checks if the number of "More Info" buttons equal the number of instances in the Disabilities page
    def test_instance_count1(self):
        print("test_instance_count1")
        # self.driver.implicitly_wait(10)
        url = "https://api.inclusiability.me/disability/all"
        response = requests.get(url).json()
        self.driver.find_element(By.LINK_TEXT, "Disabilities").click()
        self.wait.until(EC.visibility_of_element_located((By.CLASS_NAME, "model-card-row")))
        try:
            num_instances = int(
                re.search(r"Instances: (\d+)", self.driver.page_source).group(1)
            )
        except:
            print("Instances text could not be found.")
        buttons = self.driver.find_elements(By.LINK_TEXT, "More Info")
        self.assertEqual(len(buttons), 15)
        self.assertEqual(response["total"], num_instances)

    # Checks if the number of "More Info" buttons equal the number of instances in the Places page
    def test_instance_count2(self):
        print("test_instance_count2")
        url = "https://api.inclusiability.me/locations/all"
        response = requests.get(url).json()
        self.driver.find_element(By.LINK_TEXT, "Events/Accessible Places").click()
        self.wait.until(EC.visibility_of_element_located((By.CLASS_NAME, "model-card-row")))
        try:
            num_instances = int(
                re.search(r"Instances: (\d+)", self.driver.page_source).group(1)
            )
        except:
            print("Instances text could not be found.")
        buttons = self.driver.find_elements(By.LINK_TEXT, "More Info")
        self.assertEqual(len(buttons), 15)
        self.assertEqual(response["total"], num_instances)

    # Checks if the number of "More Info" buttons equal the number of instances in the first Resources page
    def test_instance_count3(self):
        print("test_instance_count3")
        url = "https://api.inclusiability.me/resources/all"
        response = requests.get(url).json()
        self.driver.find_element(By.LINK_TEXT, "Resources").click()
        self.wait.until(EC.visibility_of_element_located((By.CLASS_NAME, "model-card-row")))
        try:
            num_instances = int(
                re.search(r"Instances: (\d+)", self.driver.page_source).group(1)
            )
        except:
            print("Instances text could not be found.")
        buttons = self.driver.find_elements(By.LINK_TEXT, "More Info")
        self.assertEqual(len(buttons), 15)
        self.assertEqual(response["total"], num_instances)

    # Checks if each of the "More Info" buttons redirect to the correct URLs in the Disabilities page
    def test_instances1(self):
        print("test_instances1")
        self.driver.find_element(By.LINK_TEXT, "Disabilities").click()
        self.wait.until(EC.visibility_of_all_elements_located((By.LINK_TEXT, "More Info")))
        buttons = self.driver.find_elements(By.LINK_TEXT, "More Info")
        for i in range(len(buttons)):
            try:
                self.wait.until(EC.element_to_be_clickable((By.LINK_TEXT, "More Info")))
                buttons[i].click()
                # self.wait.until(EC.url_changes(URL + "resources/"))
                url = URL + r"disabilities/([0-9]+)"
                instance_num = re.search("([0-9]+)", self.driver.current_url)[0]

                self.wait.until(EC.url_matches(url))
                self.assertEqual(self.driver.current_url, URL + "disabilities/" + instance_num)
                self.driver.find_element(By.LINK_TEXT, "Disabilities").click()

                self.wait.until(EC.visibility_of_all_elements_located((By.LINK_TEXT, "More Info")))
                buttons = self.driver.find_elements(By.LINK_TEXT, "More Info")
            except:
                pass

    # Checks if each of the "More Info" buttons redirect to the correct URLs in the Places page
    def test_instances2(self):
        print("test_instances2")
        self.driver.find_element(By.LINK_TEXT, "Events/Accessible Places").click()
        self.wait.until(EC.visibility_of_all_elements_located((By.LINK_TEXT, "More Info")))
        buttons = self.driver.find_elements(By.LINK_TEXT, "More Info")
        for i in range(len(buttons)):
            try:
                self.wait.until(EC.element_to_be_clickable((By.LINK_TEXT, "More Info")))
                buttons[i].click()
                # self.wait.until(EC.url_changes(URL + "resources/"))
                url = URL + r"locations/([0-9]+)"
                instance_num = re.search("([0-9]+)", self.driver.current_url)[0]

                self.wait.until(EC.url_matches(url))
                self.assertEqual(self.driver.current_url, URL + "locations/" + instance_num)
                self.driver.find_element(By.LINK_TEXT, "Events/Accessible Places").click()

                self.wait.until(EC.visibility_of_all_elements_located((By.LINK_TEXT, "More Info")))
                buttons = self.driver.find_elements(By.LINK_TEXT, "More Info")
            except:
                pass


    # Checks if each of the "More Info" buttons redirect to the correct URLs in the Resources page
    def test_instances3(self):
        print("test_instances3")
        self.driver.find_element(By.LINK_TEXT, "Resources").click()
        self.wait.until(EC.visibility_of_all_elements_located((By.LINK_TEXT, "More Info")))
        buttons = self.driver.find_elements(By.LINK_TEXT, "More Info")
        for i in range(len(buttons)):
            try:
                self.wait.until(EC.element_to_be_clickable((By.LINK_TEXT, "More Info")))
                buttons[i].click()
                # self.wait.until(EC.url_changes(URL + "resources/"))
                url = URL + r"resources/([0-9]+)"
                instance_num = re.search("([0-9]+)", self.driver.current_url)[0]

                self.wait.until(EC.url_matches(url))
                self.assertEqual(self.driver.current_url, URL + "resources/" + instance_num)
                self.driver.find_element(By.LINK_TEXT, "Resources").click()

                self.wait.until(EC.visibility_of_all_elements_located((By.LINK_TEXT, "More Info")))
                buttons = self.driver.find_elements(By.LINK_TEXT, "More Info")
            except:
                pass


if __name__ == "__main__":
    unittest.main()
